EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:C C?
U 1 1 5D542057
P 4650 3800
AR Path="/5D542057" Ref="C?"  Part="1" 
AR Path="/5D53A6C9/5D542057" Ref="C?"  Part="1" 
AR Path="/5D69EB35/5D542057" Ref="C19"  Part="1" 
AR Path="/5D98E79D/5D542057" Ref="C22"  Part="1" 
F 0 "C19" H 4765 3846 50  0000 L CNN
F 1 "1u" H 4765 3755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4688 3650 50  0001 C CNN
F 3 "" H 4650 3800 50  0001 C CNN
F 4 "Kemet" H 4650 3800 50  0001 C CNN "Manufacturer"
F 5 "C0603C105K8PACTU" H 4650 3800 50  0001 C CNN "Part Number"
	1    4650 3800
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5D54205D
P 5000 3800
AR Path="/5D54205D" Ref="C?"  Part="1" 
AR Path="/5D53A6C9/5D54205D" Ref="C?"  Part="1" 
AR Path="/5D69EB35/5D54205D" Ref="C20"  Part="1" 
AR Path="/5D98E79D/5D54205D" Ref="C23"  Part="1" 
F 0 "C20" H 5115 3846 50  0000 L CNN
F 1 "10n" H 5115 3755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 5038 3650 50  0001 C CNN
F 3 "" H 5000 3800 50  0001 C CNN
F 4 "TDK" H 5000 3800 50  0001 C CNN "Manufacturer"
F 5 "CGA1A2X7R1A103K030BA" H 5000 3800 50  0001 C CNN "Part Number"
	1    5000 3800
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5D542063
P 6600 3600
AR Path="/5D542063" Ref="C?"  Part="1" 
AR Path="/5D53A6C9/5D542063" Ref="C?"  Part="1" 
AR Path="/5D69EB35/5D542063" Ref="C21"  Part="1" 
AR Path="/5D98E79D/5D542063" Ref="C24"  Part="1" 
F 0 "C21" H 6715 3646 50  0000 L CNN
F 1 "1u" H 6715 3555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6638 3450 50  0001 C CNN
F 3 "" H 6600 3600 50  0001 C CNN
F 4 "Kemet" H 6600 3600 50  0001 C CNN "Manufacturer"
F 5 "C0603C105K8PACTU" H 6600 3600 50  0001 C CNN "Part Number"
	1    6600 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5D542069
P 6200 3500
AR Path="/5D542069" Ref="R?"  Part="1" 
AR Path="/5D53A6C9/5D542069" Ref="R?"  Part="1" 
AR Path="/5D69EB35/5D542069" Ref="R3"  Part="1" 
AR Path="/5D98E79D/5D542069" Ref="R5"  Part="1" 
F 0 "R3" H 6270 3546 50  0000 L CNN
F 1 "48.7k" H 6270 3455 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 6130 3500 50  0001 C CNN
F 3 "" H 6200 3500 50  0001 C CNN
F 4 "Yageo" H 6200 3500 50  0001 C CNN "Manufacturer"
F 5 "RC0201FR-0748K7L" H 6200 3500 50  0001 C CNN "Part Number"
	1    6200 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 3300 5150 3300
Wire Wire Line
	5150 3300 5150 3400
Connection ~ 5150 3400
Wire Wire Line
	5150 3400 5250 3400
Wire Wire Line
	5250 3500 5150 3500
Wire Wire Line
	5150 3500 5150 3400
Wire Wire Line
	5550 4000 5550 4100
Wire Wire Line
	5000 3950 5000 4100
Wire Wire Line
	5000 4100 5250 4100
Connection ~ 5550 4100
Wire Wire Line
	5250 3800 5250 4100
Connection ~ 5250 4100
Wire Wire Line
	5250 4100 5550 4100
Wire Wire Line
	5000 3650 5000 3600
Wire Wire Line
	5000 3600 5250 3600
Wire Wire Line
	4650 3950 4650 4100
Wire Wire Line
	4650 4100 5000 4100
Connection ~ 5000 4100
$Comp
L Device:R R?
U 1 1 5D542088
P 6200 3900
AR Path="/5D542088" Ref="R?"  Part="1" 
AR Path="/5D53A6C9/5D542088" Ref="R?"  Part="1" 
AR Path="/5D69EB35/5D542088" Ref="R4"  Part="1" 
AR Path="/5D98E79D/5D542088" Ref="R6"  Part="1" 
F 0 "R4" H 6270 3946 50  0000 L CNN
F 1 "12.1k" H 6270 3855 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 6130 3900 50  0001 C CNN
F 3 "" H 6200 3900 50  0001 C CNN
F 4 "Yageo" H 6200 3900 50  0001 C CNN "Manufacturer"
F 5 "RC0201FR-0712K1L" H 6200 3900 50  0001 C CNN "Part Number"
	1    6200 3900
	1    0    0    -1  
$EndComp
Connection ~ 5150 3300
Wire Wire Line
	4650 3300 4650 3650
Wire Wire Line
	4650 3300 5150 3300
$Comp
L bids_symbol:LP38798-ADJ U?
U 1 1 5D542091
P 5550 3550
AR Path="/5D542091" Ref="U?"  Part="1" 
AR Path="/5D53A6C9/5D542091" Ref="U?"  Part="1" 
AR Path="/5D69EB35/5D542091" Ref="U16"  Part="1" 
AR Path="/5D98E79D/5D542091" Ref="U17"  Part="1" 
F 0 "U16" H 5550 4065 50  0000 C CNN
F 1 "LP38798-ADJ" H 5550 3974 50  0000 C CNN
F 2 "000_footprint:LP38798_WSON-12-1EP_4x4mm_P0.5mm_EP2.6x3mm_ThermalVias" H 5550 3550 50  0001 C CNN
F 3 "" H 5550 3550 50  0001 C CNN
F 4 "Texas Instruments" H 5550 3550 50  0001 C CNN "Manufacturer"
F 5 "LP38798SD-ADJ/NOPB" H 5550 3550 50  0001 C CNN "Part Number"
	1    5550 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 3700 6200 3700
Wire Wire Line
	6200 3700 6200 3650
Wire Wire Line
	6200 3750 6200 3700
Connection ~ 6200 3700
Wire Wire Line
	6200 4050 6200 4100
Wire Wire Line
	6200 4100 5850 4100
Wire Wire Line
	5850 3300 5950 3300
Wire Wire Line
	6600 3450 6600 3300
Wire Wire Line
	5850 3400 5950 3400
Wire Wire Line
	5950 3400 5950 3300
Connection ~ 5950 3300
Wire Wire Line
	5950 3300 6600 3300
Wire Wire Line
	5850 3500 5950 3500
Wire Wire Line
	5950 3500 5950 3400
Connection ~ 5950 3400
Wire Wire Line
	5850 3600 6050 3600
Wire Wire Line
	6050 3600 6050 3350
Wire Wire Line
	6050 3350 6200 3350
Wire Wire Line
	6600 3750 6600 4100
Wire Wire Line
	6600 4100 6200 4100
Connection ~ 6200 4100
Wire Wire Line
	5850 3800 5850 4100
Connection ~ 5850 4100
Wire Wire Line
	5850 4100 5550 4100
Wire Wire Line
	6600 3300 6900 3300
Connection ~ 6600 3300
Wire Wire Line
	4500 3300 4650 3300
Connection ~ 4650 3300
Text HLabel 6900 3300 2    50   Output ~ 0
Vout6V0
Text HLabel 4500 3300 0    50   Input ~ 0
Vin6V3
Text HLabel 4500 4100 0    50   Input ~ 0
GND
Wire Wire Line
	4500 4100 4650 4100
Connection ~ 4650 4100
$EndSCHEMATC
